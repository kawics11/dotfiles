if [ -d $HOME/.local/bin ] ; then
    PATH=$HOME/.local/bin:$PATH
fi
if [ -d $HOME/.local/bin/statusbar ] ; then
    PATH=$HOME/.local/bin/statusbar:$PATH
fi
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CACHE_HOME=$HOME/.cache
export EDITOR=vim
#export VIMRUNTIME=$XDG_CONFIG_HOME/vim
export TERMINAL=st
export HISTFILE=$HOME/.local/share/ash_history
export LESSHISTFILE="-"
export LESS="-R"
export LESS_TERMCAP_mb=$'\033[1;31m'
export LESS_TERMCAP_md=$'\033[1;36m'
export LESS_TERMCAP_me=$'\033[0m'
export LESS_TERMCAP_so=$'\033[01;44;33m'
export LESS_TERMCAP_se=$'\033[0m'
export LESS_TERMCAP_us=$'\033[1;32m'
export LESS_TERMCAP_ue=$'\033[0m'
if [ -f $XDG_CONFIG_HOME/shrc ] ; then
    export ENV=$XDG_CONFIG_HOME/shrc 
    source $ENV
fi
if [ -f $XDG_CONFIG_HOME/sh_logout ] ; then
    trap '. $XDG_CONFIG_HOME/sh_logout; exit' 0
fi
[ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ] && sx && exit
