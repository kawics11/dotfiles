set number relativenumber
set smartindent
set smartcase
set undodir=~/.local/share/vimundo
set undofile
set incsearch

filetype plugin on
syntax on
colorscheme solarized
set background=dark
set cursorline
set colorcolumn=80

map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
